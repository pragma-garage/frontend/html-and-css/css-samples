# Ejemplos de uso de CSS

> **Versión:** v0.1.0

En este repositorio se encuentra construido como un proyecto incremental en donde partiendo desde lo más elemental de CSS se van agregando elementos a estilización de un sitio sencillo.

## Instrucciones de uso

Cada uno de los ejemplos se fueron generando como tags en el repositorio, por lo que para acceder cualquiera de ellos bastará con clonar o bien hacer un checkout al tag relacionado.

~~~bash
#!/bin/bash

# Clonar el repositorio
git clone -b $taget_tag https://gitlab.com/pragma-garage/frontend/html-and-css/css-samples

# Cambiar al tag deseado
git checkout $target_tag
~~~

> Es necesario que el proyecto se abra en el editor VSCode y se ejecute mediante la extensión [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).
>
> La configuración que se encuentra en el archivo `.vscode/settings.json` apunta a que cuando se inicie el servidor de prueba, se ejecute en [localhost:3000](http://localhost:3000/), sirviendo el contenido desde la carpeta `public`.

## Listado de TAGS

- ...
